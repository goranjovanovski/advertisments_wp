<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'adverts');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7w*.GyxXG)[ABbqSXn&p`BM0/UiL<*}uwB!gtW?Qp= qqFhs^Elr<Ux$0OQ)8YwJ');
define('SECURE_AUTH_KEY',  '1cV/`K3oVj3ctTIy[;H0a&cP)lG_h-Ixa}3rZP2g+u@<5ly$djY};ijqJ!icLN_S');
define('LOGGED_IN_KEY',    'hFbMukuwhu)6#Cj>J1x-(a<-$TvSm8FWXrJJSQO3SpF22Pw4=M1<dXO*xI1q__23');
define('NONCE_KEY',        'L|Ln OMR`]r:1Ww7q?GrU]aV9qE! D)Zxg|w/qi1;+SP65,sABu654ajy-B2.|@o');
define('AUTH_SALT',        '|Bc-_F@dY1OO|F`m|ekAF }_6L,}8h/1USgUf41jF_T4n$yOKFE7q~cp0)a$za.[');
define('SECURE_AUTH_SALT', ',G=.fmYrs]fhPOIET#DdEnf-]4ZwqpC}c,UFl:wnz[g3UnN6*><Yl9`0{0cx`)~<');
define('LOGGED_IN_SALT',   '.~d}Gog5 5cI3ZPEGUh-eTJN~p~+Z>mMW>qD{SF1UXwf[pX3r7F9BUb89k1E{tL^');
define('NONCE_SALT',       'Dh~HWFCp-?}j>f+&Kylqxu?zJS/Fm$ukPo!%C6 ivZcDkt0t$WV+CTezTk^e@jz@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'adv_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
