<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">

	<?php if ( have_posts() ) : ?>
		<select name="filter" id="filter">
			<option value="" disabled="disabled" selected="selected">Choose filter</option>
		<?php $taxonomies = get_object_taxonomies('listing');
			foreach ($taxonomies as $key => $taxonomy) :
				echo '<option value="'.strtolower($taxonomy).'">'.strtolower($taxonomy).'</option>';
			endforeach;
		 ?>
		</select>
		<header class="page-header">
			<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>
		</header><!-- .page-header -->
	<?php endif; ?>
	

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php

	$postsPerPage = 10;
	$page = 1;
	$query = new WP_Query(array(
    'post_type' => 'listing',
    'post_status' => 'publish',
    'orderby' => 'publish_date',
    'order' => 'DESC',
    'paged' => $page,
    'posts_per_page' => $postsPerPage));

		if ( $query->have_posts() ) : ?>
			<?php
			/* Start the Loop */
			while ( $query->have_posts() ) : $query->the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/post/content', get_post_format() );

			endwhile;

			the_posts_pagination( array(
				'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
			) );

		else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

</div><!-- .wrap -->

<?php get_footer();
