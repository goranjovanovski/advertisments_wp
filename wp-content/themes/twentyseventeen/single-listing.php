<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
			$taxonomyArray = [];
				$taxonomies = get_post_taxonomies( $post->ID );
				foreach ($taxonomies as $taxonomy) {
					$terms = get_the_terms(  $post->ID,  $taxonomy )[0];
					// var_dump($terms);
					$taxonomyArray[] = ['location'=>$terms->taxonomy, 'value'=>$terms->name];
					$taxonomy_query = new WP_Query( array(
				    'tax_query' => array(
				        array(
				            'taxonomy' => $taxonomy,
				            'field' => 'id',
				            'terms' => $terms->term_id,
						     ),
						    ),
						) );				
						}
					//var_dump($taxonomyArray);
					foreach ($taxonomyArray as $key => $value) {
						echo '<p>'.$value['location'].': '. $value['value'].'</p>' ;
					}
				echo 'Date: '.$taxonomy_query->posts[0]->post_date . '</p>';
				get_template_part( 'template-parts/post/listing');

			endwhile; // End of the loop.
			?>
			<p></p>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
