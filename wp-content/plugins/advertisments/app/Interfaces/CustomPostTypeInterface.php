<?php

/**
 * undocumented class
 *
 * @package default
 * @author 
 **/

namespace App\Interfaces;

interface CustomPostTypeInterface 
{
	
    public function registerThePostType( $param = [] );
    public function registerTheTaxonomy( $param = [] );

}