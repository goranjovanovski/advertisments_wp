<?php

/**
 * 
 */
namespace App\Interfaces;

interface FilterHookInterface 
{

	/**
	 * get filters hook
	 *
	 * @return array
	 **/
	
	public static function get_filters();


}