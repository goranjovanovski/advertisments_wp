<?php

/**
 * @package Advertisments
 */

namespace App\Interfaces;

interface ActionHookInterface 
{

	/**
	 * get actions hook
	 *
	 * @return array
	 **/

	public static function get_actions();
	


}