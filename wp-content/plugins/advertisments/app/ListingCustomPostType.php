<?php
/**
 * @package Advertisments
 **/

namespace App;

use App\Interfaces\CustomPostTypeInterface;
use App\Interfaces\ActionHookInterface;
// use App\Interfaces\FilterHookInterface;
// 

/**
 *
 * @package default
 * @author 
 **/


class ListingCustomPostType extends PluginHooksManager implements CustomPostTypeInterface, ActionHookInterface
{
	const PLUGIN_NAME = 'Listing';
	public $taxonomies = [
		'Location',
		'Price'
	];


	public function registerThePostType($args = null){

		$singular = self::PLUGIN_NAME;
		$plural = self::PLUGIN_NAME.'s';

		$args = array(
			'labels' => array(
                'name' => __( $plural, 'post type general name' ),
                'singular_name' => __( $singular, 'post type singular name' ),
                'add_new' => __( 'Add New '.$singular ),
                'add_new_item' => __( 'Add New '.$singular ),
                'edit_item' => __( 'Edit '.$singular ),
                'new_item' => __( 'New '.$singular ),
                'view_item' => __( 'View '.$singular ),
                'search_items' => __( 'Search '.$plural ),
                'not_found' =>  __( 'No '.$plural.' found in search' ),
                'not_found_in_trash' => __( 'No '.$plural.' found in Trash' ),
			),
			'public' => true, 
			'has_archive' => true,
			'show_ui' => true,
			'_builtin' => false,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => $singular),
			'query_var' => $singular,
			'taxonomies' =>  $this->taxonomies, // Add tags and categories taxonomies
			'supports' => array('title','editor','author','comments')
        );
		 register_post_type(self::PLUGIN_NAME, $args);
		 // register_post_type(self::PLUGIN_NAME, ['public' => true, 'label' => self::PLUGIN_NAME.'s']);
		 
	}

	public static function get_actions(){
		return [
			'init' => ['registerThePostType','registerTheTaxonomy'],
		];
	}

	public function registerTheTaxonomy( $args = null ){
// die('tt');

		foreach ($this->taxonomies as $taxonomy) {
		    $labels = array(
                'name'              => _x( $taxonomy, 'taxonomy general name' ),
                'singular_name'     => _x( $taxonomy, 'taxonomy singular name' ),
                'search_items'      => __( 'Search ' . $taxonomy . 's'),
                'all_items'         => __( 'All ' . $taxonomy. 's' ),
                'parent_item'       => __( 'Parent ' . $taxonomy ),
                'parent_item_colon' => __( 'Parent ' . $taxonomy.':'),
                'edit_item'         => __( 'Edit ' . $taxonomy),
                'update_item'       => __( 'Update ' . $taxonomy),
                'add_new_item'      => __( 'Add New ' . $taxonomy),
                'new_item_name'     => __( 'New ' . $taxonomy . ' Name' )
                );
            $args = array(               
                'labels'  => $labels,
                'hierarchical' => true,
                'query_var' => true,
                'public' => true,
                'rewrite' => array('slug' => $taxonomy),
                'show_in_nav_menus' => true,
                'show_ui' => true,
                'show_tagcloud' => true
                );

			
			register_taxonomy( $taxonomy, strtolower(self::PLUGIN_NAME), $args );
		}

	}

}