<?php

/**
 * class that handles registering and generating the add_action and add_filter hooks
 *
 * @package Advertisments
 * @author 
 **/

namespace App;

use App\Interfaces\ActionHookInterface;
use App\Interfaces\FilterHookInterface;

class PluginHooksManager
{
	/**
	 * undocumented function
	 *
	 * @param mixed $object 
	 **/
	public function register($object)
	{
		if($object instanceof ActionHookInterface ){
			$this->register_actions($object);
		}

		if($object instanceof FilterHookInterface){
			$this->register_filters($object);
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 **/
	public function register_actions(ActionHookInterface $object)
	{
		foreach ($object->get_actions() as $name => $function_name) {
			$this->generate_the_action($object, $name, $function_name);
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 **/
	public function register_filters(FilterHookInterface $object)
	{
		foreach ($object->get_filters() as $name => $function_name) {
			$this->generate_the_filter($object, $name, $function_name);
		}
	}

	/**
	 * generating the action
	 *
	 * @param [object] $object [<description>]
	 * @param string $name [<description>]
	 * @param [type] $[name] [<description>]
	 * @author 
	 **/
	public function generate_the_action(ActionHookInterface $object, $name, $function_name)
	{
		if (is_string($function_name)){
			add_action($name, [$object, $function_name]);
		}
		elseif (is_array($function_name)) {
			foreach ($function_name as $key => $param) {
				add_action($name, [$object, $param]);
			}
			//add_action($name, array($object, $function_name[0]), isset($function_name[1]) ? $function_name[1] : 10, isset($function_name[2]) ? $function_name[2] : 1);
		}
	}

	// 	public function generate_action(ActionHookInterface $object, $name, $function_name)
	// {
	// 	if (is_string($function_name)){
	// 		// var_dump($name, $function_name); die;
	// 		var_dump(add_action($name, $function_name)); die;
	// 	}
	// }

} 
