<?php

namespace App;

/**
 * Loader class that instantiates all classes within plugin init file.
 **/

final class Loader
{

	public static function getTheClasses(){
		return [
			Activator::class,
			Deactivator::class,
			ListingCustomPostType::class,
			PluginHooksManager::class
		];
	}

	public static function instantiatesClasses(){
		foreach (self::getTheClasses() as $theClass) {
			$class = self::instantiate($theClass);
			// if(method_exists($class, 'register')){
			// 	$class->register();
			// }
		}
			$listingCPT = new ListingCustomPostType();
			$hooks = new PluginHooksManager();
			$hooks->register($listingCPT); 
	}

	public static function instantiate($theClass){
		return $class = new $theClass();
	}
}
 
