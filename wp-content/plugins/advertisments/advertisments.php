<?php
/**
 * @package Advertisments
 **/

/*
Plugin Name: Advertisments
Plugin URI: 
Description: Awesome advertisments plugin
Version: 1.0
Author: Goran Jovanovski
Author URI:
License: GPLv2 or later
 */


defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(file_exists(dirname( __FILE__ ) . '/vendor/autoload.php')){
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

if ( class_exists( 'App\\Loader' ) ) {
	App\Loader::instantiatesClasses();
}

// function activate_plugin() {
// 	Activator::activate();
// }
// register_activation_hook( __FILE__, 'activate_plugin' );


// function deactivate_plugin() {
// 	Deactivator::deactivate();
// }
// register_deactivation_hook( __FILE__, 'deactivate_plugin' );

